package com.gabitovairat.example1.network_api.data

object SearchRepositoryProvider {
    fun provideSearchRepository(): SearchRepository {
        val apiService = GithubApiService.create()
        return SearchRepository(apiService)
    }
}

