package com.gabitovairat.example1.network_api.data

import com.gabitovairat.example1.data.GitHubUserResult
import io.reactivex.Observable

class SearchRepository(val apiService: GithubApiService) {
    fun searchUsers(searchString: String, page: Int, perPage: Int): Observable<GitHubUserResult> {
        return apiService.search(searchString, page, perPage)
    }
}