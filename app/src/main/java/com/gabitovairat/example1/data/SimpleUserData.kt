package com.gabitovairat.example1.data

data class SimpleUserData(val userName: String?, val userPicUrl: String?) {
}