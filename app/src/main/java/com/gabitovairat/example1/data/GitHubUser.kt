package com.gabitovairat.example1.data

data class GitHubUser(
    public val id: Long,
    public val login: String?,
    public val email: String?) {
}

data class GitHubUserResult (
    val total_count: Int,
    val incomplete_results: Boolean,
    val items: ArrayList<GitHubUser>)