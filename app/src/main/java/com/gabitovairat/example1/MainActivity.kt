package com.gabitovairat.example1

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import com.gabitovairat.example1.data.SimpleUserData
import com.gabitovairat.example1.live_data.SimpleUserLiveData
import com.squareup.picasso.Picasso


class MainActivity : FragmentActivity() ,
    Observer<SimpleUserData?> {

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
    }

    override fun onStart() {
        super.onStart()
        navigationViewButton.setOnClickListener{
            drawer_layout.openDrawer(GravityCompat.START)
            SimpleUserLiveData.getInstance().removeObserver(this)
            SimpleUserLiveData.getInstance().observe(this,this);

            userIconImageView.setOnClickListener{
                MainActivity@this.findNavController(R.id.my_nav_host_fragment).navigate(R.id.action_global_loginFragment2)
                drawer_layout.closeDrawer(GravityCompat.START)
            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onChanged(simpleUserInfo: SimpleUserData?) {
        if (userIconImageView == null)
            return;

        if (simpleUserInfo != null) {
            if (!simpleUserInfo.userPicUrl!!.isEmpty()) {
                Picasso.with(this.baseContext).load(simpleUserInfo.userPicUrl).fit().centerCrop()
                    //.placeholder(R.drawable.user_placeholder)
                    //.error(R.drawable.user_placeholder_error)
                    .into(userIconImageView);
            } else {
                userIconImageView.setImageResource(R.drawable.ic_launcher_foreground)
            }
            userNameTextView.text = simpleUserInfo.userName;
        } else {
            userIconImageView.setImageResource(R.drawable.ic_launcher_foreground)
            userNameTextView.text = "[need to login]"
        }
    }
}