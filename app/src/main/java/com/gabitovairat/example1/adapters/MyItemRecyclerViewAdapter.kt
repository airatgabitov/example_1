package com.gabitovairat.example1.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gabitovairat.example1.R
import com.gabitovairat.example1.data.GitHubUser

import kotlinx.android.synthetic.main.fragment_github_users.view.*

class MyItemRecyclerViewAdapter(
    public var values: List<GitHubUser>
) : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_github_users, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        if (item.id == -1L) {
            holder.dataSection.visibility = View.GONE
            holder.progerssSection.visibility = View.VISIBLE
        } else {
            holder.dataSection.visibility = View.VISIBLE
            holder.progerssSection.visibility = View.GONE

            holder.idView.text = "" + position
            holder.contentView.text = item.login
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val idView: TextView = view.item_number
        val contentView: TextView = view.content
        val dataSection : View = view.dataSection
        val progerssSection : View = view.progressSection

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }
}