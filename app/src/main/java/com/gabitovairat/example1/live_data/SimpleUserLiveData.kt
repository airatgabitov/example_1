package com.gabitovairat.example1.live_data

import androidx.lifecycle.MutableLiveData
import com.gabitovairat.example1.data.SimpleUserData

public class SimpleUserLiveData : MutableLiveData<SimpleUserData?>() {
    companion object {
        private var instance: SimpleUserLiveData? = null

        fun getInstance(): SimpleUserLiveData {
            if (instance == null) {
                instance =
                    SimpleUserLiveData()
            }
            return instance!!
        }
    }
}