package com.gabitovairat.example1.live_data

import android.util.Log
import androidx.lifecycle.LiveData
import com.gabitovairat.example1.AppDecidionConfig
import com.gabitovairat.example1.data.GitHubUser
import com.gabitovairat.example1.data.GitHubUserResult
import com.gabitovairat.example1.data.GitHubUsersData
import com.gabitovairat.example1.network_api.data.SearchRepositoryProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

public class GitHubUsersLIstLiveData(value: GitHubUsersData?) : LiveData<GitHubUsersData>(value) {

    var lastGettedData : GitHubUsersData? = null
    var lastUsedSearchString: String? = null
    var isLastPageDetected: Boolean = false

    public fun runUpdateFromServer(searchString: String) {
        lastUsedSearchString = searchString
        isLastPageDetected = false

        if (lastGettedData == null)
            lastGettedData =
                GitHubUsersData(ArrayList<GitHubUser>())

        val repository = SearchRepositoryProvider.provideSearchRepository()
        repository.searchUsers(searchString, 1, AppDecidionConfig.gitHubPerPageCount)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe ({
                    result ->
                    initData(result)
                    Log.d("Result", "There are ${result.items.size} Java developers in Lagos")
            }, { error ->
                error.printStackTrace()
            })
    }

    private fun initData(result: GitHubUserResult) {
        with(lastGettedData!!) {
            usersList.clear()
            usersList.addAll(result.items)
            isLastPageDetected = result.total_count <= usersList.size
            addLoadingItemIfNeed(usersList, isLastPageDetected)
        }
        setValue(lastGettedData);
    }

    private fun removeLoadingItem(usersList: java.util.ArrayList<GitHubUser>) {
        var lastItem : GitHubUser = usersList.get(usersList.size-1);
        if (lastItem.id == -1L) {
            usersList.remove(lastItem)
        }
    }

    private fun addLoadingItemIfNeed(
        usersList: java.util.ArrayList<GitHubUser>,
        lastPageDetected: Boolean
    ) {
        if (!lastPageDetected) {
            usersList.add(GitHubUser(-1, "", ""))
        }
    }

    private fun addMoreData(result: GitHubUserResult) {
        with(lastGettedData!!){
            usersList.addAll(result.items)
            isLastPageDetected = result.total_count <= usersList.size
            addLoadingItemIfNeed(usersList, isLastPageDetected)
        }
        setValue(lastGettedData);
    }

    override fun onActive() {
        if (lastGettedData != null)
            setValue(lastGettedData)
    }

    fun getMoreItems() {
        if (lastGettedData == null || lastUsedSearchString == null || isLastPageDetected) {
            setValue(lastGettedData);
            return
        }

        removeLoadingItem(lastGettedData!!.usersList)

        val repository = SearchRepositoryProvider.provideSearchRepository()
        val pageNum = lastGettedData!!.usersList.size/AppDecidionConfig.gitHubPerPageCount + 1;

        repository.searchUsers(lastUsedSearchString!!, pageNum, AppDecidionConfig.gitHubPerPageCount)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe ({
                    result ->
                addMoreData(result)
                Log.d("Result", "There are ${result.items.size} Java developers in Lagos")
            }, { error ->
                error.printStackTrace()
            })

    }

    companion object {
        private var instance: GitHubUsersLIstLiveData? = null
        @JvmStatic
        public fun getInstance(): GitHubUsersLIstLiveData {
            if (instance == null) {
                instance =
                    GitHubUsersLIstLiveData(
                        null
                    )
            }
            return instance!!
        }

    }
}