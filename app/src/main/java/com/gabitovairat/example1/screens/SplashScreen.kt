package com.gabitovairat.example1.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.gabitovairat.example1.R
import com.gabitovairat.example1.data.SimpleUserData
import com.gabitovairat.example1.live_data.SimpleUserLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class SplashScreen : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_splash_screen, container, false)
        return view;
    }

    override fun onStart() {
        super.onStart()
        //FirebaseAuth.getInstance().signOut() //is we can't unlogin
        var user = Firebase.auth.currentUser;

        if (user != null) {
            // User is signed in.
            view?.findNavController()?.navigate(R.id.action_splashScreen_to_githubUsersFragment)
            SimpleUserLiveData.getInstance().postValue(SimpleUserData(user?.displayName, user?.photoUrl.toString()))
        } else {
            // No user is signed in.
            view?.findNavController()?.navigate(R.id.action_splashScreen_to_loginFragment)
            SimpleUserLiveData.getInstance().postValue(null)
        }
    }
}