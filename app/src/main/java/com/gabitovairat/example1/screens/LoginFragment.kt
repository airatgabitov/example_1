package com.gabitovairat.example1.screens

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.gabitovairat.example1.R
import com.gabitovairat.example1.data.SimpleUserData
import com.gabitovairat.example1.live_data.SimpleUserLiveData
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : Fragment(), Observer<SimpleUserData?> {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    val RC_SIGN_IN : Int = 123;

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        SimpleUserLiveData.getInstance().observe(viewLifecycleOwner,this);

        sign_out_button.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            Credentials.getClient((LoginFragment@this.context)!!).disableAutoSignIn();
            SimpleUserLiveData.getInstance().postValue(null)
        }

        sign_in_button.setOnClickListener {
            // Choose authentication providers
            val providers = arrayListOf(
                //AuthUI.IdpConfig.EmailBuilder().build()
                //AuthUI.IdpConfig.PhoneBuilder().build(),
                AuthUI.IdpConfig.GoogleBuilder().build()
                //AuthUI.IdpConfig.FacebookBuilder().build(),
                //AuthUI.IdpConfig.TwitterBuilder().build(),
            )

            // Create and launch sign-in intent
            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .setIsSmartLockEnabled(false)
                    .setAlwaysShowSignInMethodScreen(true)
                    .build(),
                RC_SIGN_IN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                val user = FirebaseAuth.getInstance().currentUser
                SimpleUserLiveData.getInstance().postValue(SimpleUserData(user?.displayName, user?.photoUrl.toString()))
                view?.findNavController()?.navigate(R.id.action_loginFragment_to_githubUsersFragment)
            } else {
                Toast.makeText(context, "Error on login: " + response?.error?.message, Toast.LENGTH_LONG).show()
                SimpleUserLiveData.getInstance().postValue(null)
            }
        }
    }

    override fun onChanged(simpleUserData: SimpleUserData?) {
        if (simpleUserData != null) {
            forSignOutSection.visibility = View.VISIBLE
            forSignInSection.visibility = View.GONE
            currentAccountTextView.text = simpleUserData.userName;
        } else {
            forSignOutSection.visibility = View.GONE
            forSignInSection.visibility = View.VISIBLE
        }
    }


}