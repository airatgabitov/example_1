package com.gabitovairat.example1.screens

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gabitovairat.example1.AppDecidionConfig
import com.gabitovairat.example1.adapters.MyItemRecyclerViewAdapter
import com.gabitovairat.example1.R
import com.gabitovairat.example1.data.GitHubUser
import com.gabitovairat.example1.data.GitHubUsersData
import com.gabitovairat.example1.live_data.GitHubUsersLIstLiveData
import com.gabitovairat.example1.widgets.PaginationScrollListener
import kotlinx.android.synthetic.main.fragment_github_users_list.*

/**
 * A fragment representing a list of Items.
 */
class GithubUsersFragment : Fragment(),
    Observer<GitHubUsersData?> {

    private var columnCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_github_users_list, container, false)
        return view
    }

    override fun onStart() {
        super.onStart()

        // Set the adapter
        if (list is RecyclerView) {
            with(list) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter =
                    MyItemRecyclerViewAdapter(
                        ArrayList<GitHubUser>()
                    )
                initPaginator(list)
            }
        }

        //searchEditText
        this.searchEditText.addTextChangedListener(searchTextWatcher)
        GitHubUsersLIstLiveData.getInstance().observe(this,this);
    }

    private fun initPaginator(recyclerView: RecyclerView) {
        recyclerView?.addOnScrollListener(object : PaginationScrollListener(recyclerView.layoutManager as LinearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                //you have to call loadmore items to get more data
                GitHubUsersLIstLiveData.getInstance().getMoreItems();
            }
        })
    }

    var isLoading : Boolean = false;
    var isLastPage : Boolean = false;

    var searchTextWatcher: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable) {}
        override fun beforeTextChanged(
            s: CharSequence, start: Int,
            count: Int, after: Int
        ) {
        }

        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
            updateContentByFilter()
        }
    }

    var updateFilteringDelay: Long =
        AppDecidionConfig.seachStringTypeDelayBeforRequest
    var updateFilerHandler = Handler()

    var updateFilterRunable = Runnable {
        try {
            performSearch(searchEditText.text.toString())
        } catch (e: Throwable) {}
    }

    private fun performSearch(searchString: String) {
        GitHubUsersLIstLiveData.getInstance().runUpdateFromServer(searchString)
    }

    override fun onChanged(gitHubUsersData: GitHubUsersData?) {
        isLoading = false;
        if (gitHubUsersData != null) {
            with(list.adapter as MyItemRecyclerViewAdapter) {
                values = gitHubUsersData!!.usersList
                notifyDataSetChanged()
            }
        }
    }

    fun updateContentByFilter() {
        updateFilerHandler.removeCallbacks(updateFilterRunable)
        updateFilerHandler.postDelayed(updateFilterRunable, updateFilteringDelay)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            GithubUsersFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}